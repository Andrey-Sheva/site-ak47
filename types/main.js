const g = document.getElementById('kriva').getContext('2d');
const drawBtn = document.getElementById('draw');
let path = [];//массив точек

//нулевой уровень = коробка без стенки слева
path.push({x: 0, y: 0});
path.push({x: 1, y: 0});
path.push({x: 1, y: 1});
path.push({x: 0, y: 1});
let len = 1;//размер нулевого уровня

for (let level = 0; level < 5; level++) {//цикл генерирует кривую пятого уровня
    let cnt = path.length;
    let newpath = [];

    //на каждом уровне делаем четыре копии массива точек так,
    //чтобы в итоге сохранялась правильная последовательность, и тогда в конце их можно будет просто соединить линиями

    for (let i = 0; i < cnt; i++) {//первую копию транспонируем
        newpath[i] = {};
        newpath[i].x = path[i].y;
        newpath[i].y = path[i].x;
    }

    for (let i = cnt; i < cnt * 2; i++) {//вторую копию просто смещаем вправо
        newpath[i] = {};
        newpath[i].x = path[i % cnt].x + len + 1;
        newpath[i].y = path[i % cnt].y;
    }

    for (let i = cnt * 2; i < cnt * 3; i++) {//третью копию смещаем вправо вниз
        newpath[i] = {};
        newpath[i].x = path[i % cnt].x + len + 1;
        newpath[i].y = path[i % cnt].y + len + 1;
    }

    for (let i = cnt * 3; i < cnt * 4; i++) {//четвертую копию транспонируем вокруг побочной диагонали и смещаем вниз
        newpath[i] = {};
        newpath[i].x = len - path[i % cnt].y;
        newpath[i].y = len - path[i % cnt].x + len + 1;
    }

    len = len * 2 + 1;//размер увеличился
    path = newpath;
}

let size = 8;//размер отрезка
function line(p0, p1) {
    g.beginPath();
    g.moveTo(p0.x * size + 2, p0.y * size + 2);
    g.lineTo(p1.x * size + 2, p1.y * size + 2);
    g.stroke();
}

let ind = 1;
let cnt = path.length;


function rend() {//красиво рисуем
    if (ind >= cnt) return;
    line(path[ind - 1], path[ind]);
    ind++;
    setTimeout(rend, 1);// drawing speed
}

drawBtn.onclick = () =>{
    rend();
}





